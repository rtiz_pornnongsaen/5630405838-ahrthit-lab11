<?php
$writer = new XMLWriter();
$writer->openMemory();
$writer->setIndent(true);
$writer->setIndentString("");

$writer->startDocument("1.0", "UTF-8");

$writer->startElement("BookList");
$writer->startElement("Book");
$writer->writeElement("Title", "jQuery: Novice to Ninja");
$writer->writeElement("Publisher", "Site point");
$writer->endElement();

$writer->startElement("Book");
$writer->writeElement("Title", "Learning jQuery");
$writer->writeElement("Publisher", "PACKT");
$writer->endElement();

$writer->startElement("Book");
$writer->writeElement("Title", "Head First jQuery");
$writer->writeElement("Publisher", "O'Reilly");
$writer->endElement();

$writer->startElement("Book");
$writer->writeElement("Title", "jQuery UT 1.8");
$writer->writeElement("Publisher", "PACKT");
$writer->endElement();

$writer->endDocument();
header("Content-type: text/xml");
echo $writer->outputMemory();
?>