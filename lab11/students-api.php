<?php

$data = array(
    "students" => array(
        array(
            "name" => "Ahrthit",
            "books" => array(
                "One Piece",
                "Wireless For DUMMIES",
                "Penetration Testing"
            ),
            "education" => array(
                array(
                    "high school" => "Sirindhorn School"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )),
        array(
            "name" => "ชนพัฒน์",
            "books" => array(
                "ภาพปริศนา",
                "พระพุทธเจ้า",
                "หมากกระดาน"
            ),
            "education" => array(
                array(
                    "high school" => "Satit KKU"
                ),
                array(
                    "undergrad school" => "Khon Kaen University"
                )
            )
        )
    )
);

if ($_GET["alt"] == 'xml') {
    header('Content-Type: application/xml; charset=utf-8');
    echo '<?xml version="1.0" encoding="UTF-8" ?> '
            . '<students>'
                . '<student>'
                    . '<name>Ahrthit</name>'
                    . '<books>'
                        . '<book>One Piece</book>'
                        . '<book>Wireless For DUMMIES</book>'
                        . '<book>Penetration Testing</book>'
                    . '\</books>'
                    . '<education>'
                        . '<highschool>Sirindhorn School</highschool>'
                        . '<undergradschool>Khon Kaen University</undergradschool>'
                    . '</education>'
                . '</student>'
                . '<student>'
                    . '<name>ชนพัฒน์</name>'
                    . '<books>'
                        . '<book>ภาพปริศนา</book>'
                        . '<book>พระพุทธเจ้า</book>'
                        . '<book>หมากกระดาน</book>'
                    . '</books>'
                    . '<education>'
                        . '<highschool>Satit KKU</highschool>'
                        . '<undergradschool>Khon Kaen University"</undergradschool>'
                    . '</education>'
                . '</student>'
            . '</students>';
    
} else {
    header('Content-Type: application/json');
    echo json_encode($data);
}
?>